#!/usr/bin/env bash


while [ 1 ]
do
ping -c 1 10.13.0.24 || {
    echo "Ok, failed to ping rpi"
    notify-send "CAN'T REACH RPI $(date +%T)"
}
sleep 5
done
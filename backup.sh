#!/usr/bin/env bash

SOTA_DIR=/var/sota
BACKUPS_DIR=/var/sota/backup

BACKUPS=$(ls -t $BACKUPS_DIR)

function Dump_Backups() {
	echo "Backups available:"
	for backup in ${BACKUPS[@]}; do
		echo "$(cat $BACKUPS_DIR/$backup/gateway.url) - $(cat $BACKUPS_DIR/$backup/info.json |jq -r .registeredName) - $(cat $BACKUPS_DIR/$backup/info.json |jq -r .deviceUuid)"
	done

}

function Backup_Current {
	backup=$BACKUPS_DIR/$(cat $SOTA_DIR/import/info.json | jq -r .deviceUuid)-$(cat $SOTA_DIR/import/info.json | jq -r .registeredName)
	mkdir -p $backup || {
		echo "Failed to create backup dir: $backup"
		echo "Stopping."
		exit -1
	}

	systemctl stop aktualizr
	mv $SOTA_DIR/import/* $backup/
	mv $SOTA_DIR/sql.db $backup/
	sync
}


function Swap_Server {
	DEVICE_UUID=$1
	MATCHING_BACKUP=""
	for backup in ${BACKUPS[@]}; do
        if [ "$(cat $BACKUPS_DIR/$backup/info.json |jq -r .deviceUuid)" == "$DEVICE_UUID" ]; then

            echo "found matching backup: $(cat $BACKUPS_DIR/$backup/info.json |jq -r .registeredName) on $(cat $BACKUPS_DIR/$backup/gateway.url) "
   			MATCHING_BACKUP=$backup 
			break
        fi
    done
	
	# What if it doesn't match!!!
	if [ -z "$MATCHING_BACKUP" ]; then
		echo "No matching backup found"
		exit -1
	fi

	echo "Stopping aktualizr and backing up existing deployment..."
	Backup_Current
	
	
	echo "Copying in new configuration and restarting aktualizr"
	mv $BACKUPS_DIR/$MATCHING_BACKUP/* $SOTA_DIR/import/ && mv $SOTA_DIR/import/sql.db $SOTA_DIR/
	sync

	systemctl restart aktualizr

	echo "Removing backup of the now current configuration"
	rm -r $BACKUPS_DIR/$MATCHING_BACKUP
}

if [ "$1" == "backup" ]; then
	Backup_Current
	exit 0
fi

if [ "$1" == "ls" ]; then
	Dump_Backups
	exit 0
fi


if [ "$1" == "swap" ]; then
	if [ -z "$2" ]; then
		echo "Please pass in device-uuid of available backup configuration to use"
		exit -1
	fi
	Swap_Server $2
	exit 0
fi

echo "This script is intended to be run on TorizonCore"
echo "./backup.sh [ls | swap <device-uuid> ]
	backup - backup current configuration
	ls - list all available device backups
	swap - Backup existing configuration (if it exists) and set the specified backup as the active configuration.
	     -- device-uuid comes from listing the available backups"

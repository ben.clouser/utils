#!/usr/bin/env sh




AWS_ACCESS_ID=$1
AWS_ACCESS_KEY=$2
AWS_USER_POOL_ID=$3
CLIENT_ID=$4
CMD=$5
USER_NAME=$6
USER_PASSWORD=$7 # Exp3lli@rmus


OUT_FILE=user-info.json
ACCESS_FILE=access.json


set -e
# ./idp-user.sh AWS_ACCESS_ID AWS_ACCESS_KEY AWS_USER_POOL CMD USER_NAME USER_PASSWORD OUT_FILE

if [ "$CMD" = "create" ];then
    aws --region us-east-1 cognito-idp admin-create-user --username $USER_NAME --user-pool-id $AWS_USER_POOL_ID > $OUT_FILE
    aws --region us-east-1 cognito-idp admin-set-user-password --username $USER_NAME --password $USER_PASSWORD --permanent --user-pool-id $AWS_USER_POOL_ID
    # get jwt token
    USERNAME=$(cat $OUT_FILE | jq .User.Username)
    aws --region us-east-1 cognito-idp admin-initiate-auth --auth-flow ADMIN_NO_SRP_AUTH --user-pool-id $AWS_USER_POOL_ID --client-id $CLIENT_ID --auth-parameters USERNAME=$USERNAME,PASSWORD="$USER_PASSWORD" > $ACCESS_FILE
    ACCESS_TOKEN=$(cat $ACCESS_FILE | jq .AuthenticationResult.AccessToken)
    echo "Successfully created user: $USERNAME"
    echo "Access token: $ACCESS_TOKEN"
elif [ "$CMD" = "delete" ]; then
    aws --region us-east-1 cognito-idp admin-delete-user --username $USER_NAME --user-pool-id $AWS_USER_POOL_ID
else
    echo "Unkown command. Should be one of create|delete"
fi

set +e

#aws --region us-east-1 cognito-idp list-users --user-pool-id us-east-1_Q5QdAyeZf | jq .Users > cognito-users.json
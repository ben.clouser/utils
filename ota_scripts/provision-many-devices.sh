#!/usr/bin/env bash

AUTH_TOKEN=$1
NUMBER_DEVICES=$2

for num in $( seq 0 $NUMBER_DEVICES )
do
	echo "creating device # ${num}"
	./provision-device.sh $AUTH_TOKEN
done



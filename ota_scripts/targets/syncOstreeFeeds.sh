#!/usr/bin/env bash

# Script assumes an itialized ostree repo:
# ostree init --repo=./ostree_repo --mode=archive
# ostree --repo=./ostree_repo remote add --no-gpg-verify toradex-ostree http://feeds1.toradex.com/ostree

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

FEEDS_DIR=./


DRY_RUN=false

if [ -n $1 ] && [ "--dry-run" == "$1" ]; then
	echo "performing dry-run"
	DRY_RUN=true
fi


echo "script dir is $SCRIPT_DIR"
source $SCRIPT_DIR/common.sh

numPulledBranches=0
skippedBranches=()
numSkippedBranches=0
for machine in ${MACHINES[@]}
do
	echo "== Starting ostree sync for $machine"
	distros=( $(GetDistros $machine) )
	if [ -z "$distros" ]; then
		echo ""
		echo "WARNING WARNING WARNING!!!"
		echo "No distros found for $machine!!! SKIPPING"
		echo ""
		skippedPkgs=$((skippedPkgs + 1))
		continue
	fi
	for distro in ${distros[@]}
	do
		for image in ${IMAGES[@]}
		do
			for buildPurpose in ${BUILD_PURPOSES[@]}
			do
				echo "- ${OE_VERSION}/${machine}/${distro}/${image}/${buildPurpose}"
				OSTREE_BRANCH="${OE_VERSION}/${machine}/${distro}/${image}/${buildPurpose}"
				if ! $DRY_RUN; then
					ostree --repo=${FEEDS_DIR}/ostree_repo pull --depth=-1 --mirror toradex-ostree $OSTREE_BRANCH || {
						echo "WARNING!"
						echo "Failed to sync $OSTREE_BRANCH. Skipping"

						skippedBranches+=("$OSTREE_BRANCH")
						numSkippedBranches=$((numSkippedBranches + 1))
						continue
					}
				fi
				numPulledBranches=$((numPulledBranches + 1))
			done
		done
	done
	echo ""
done


echo ""
echo "OSTree branches pulled: $numPulledBranches. OSTree branches skipped(or failed): $numSkippedBranches"
echo "Branches skipped: "
for branch in ${skippedBranches[@]}
do
echo "${branch}"
done



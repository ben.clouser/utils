OE_VERSION="5"
MACHINES=( "apalis-imx8" \
"apalis-imx6" \
"colibri-imx8x" \
"colibri-imx7-emmc" \
"colibri-imx6" \
"verdin-imx8mm" \
"verdin-imx8mp" \
"colibri-imx6ull" \
"apalis-imx8x" \
"colibri-imx7" \
"colibri-imx8x-v10b" )

# Each module supports different distros. The chief distinction is whether Torizoncore is built via upstream or not
function GetDistros {
	MODULE=$1
	case "$MODULE" in
	"apalis-imx6" | "colibri-imx7-emmc" | "colibri-imx6" | "colibri-imx6ull" | "colibri-imx7" )
		echo "torizon-upstream torizon-upstream-rt"
			;;
	"apalis-imx8" | "colibri-imx8x" | "verdin-imx8mm" | "verdin-imx8mp" | "apalis-imx8x" | "colibri-imx8x-v10b")
		echo "torizon torizon-rt"
		;;
	*)
	esac
		echo ""
}

IMAGES=( "torizon-core-docker" "torizon-core-podman" ) 

BUILD_PURPOSES=( "nightly" "monthly" "release" )

# A good example function for looping through all ostree branches
function PrintPackages {
    publishedPkgs=0
    skippedPkgs=0
    for machine in ${MACHINES[@]}
    do
        distros=( $(GetDistros $machine) )
        if [ -z "$distros" ]; then
            echo ""
            echo "WARNING WARNING WARNING!!!"
            echo "No distros found for $machine!!! SKIPPING"
            echo ""
            skippedPkgs=$((skippedPkgs + 1))
            continue
        fi
        for distro in ${distros[@]}
        do
            for image in ${IMAGES[@]}
            do
                for buildPurpose in ${BUILD_PURPOSES[@]}
                do
                    echo "- ${OE_VERSION}/${machine}/${distro}/${image}/${buildPurpose}"
                    publishedPkgs=$((publishedPkgs + 1))
                done
            done
        done
        echo ""
    done

    echo "Done: Published ${publishedPkgs} and skipped ${skippedPkgs}"
}

function ParsePlatform {
OSTREE_BRANCH=$1
IFS='/'     # hyphen (-) is set as delimiter
read -ra PIECES <<< "$OSTREE_BRANCH"   # str is read into an array as tokens separated by IFS
echo ${PIECES[1]}
IFS=' '     # reset to default value after
}
#!/usr/bin/env bash

./syncOstreeFeeds.sh || {
  echo "Failed to sync feeds. Stopping"
  exit -1
}

echo "\n\n=== Pushing all to app-pilot.int.toradex.com..."
./pushAllToCluster.sh app-pilot.torizon.io $JWT_PROD || {
	echo "Failed to publish targets to PILOT cluster"
}

echo "\n\n=== Pushing all to app-flat.int.toradex.com..."
./pushAllToCluster.sh app-flat.int.toradex.com $JWT_FLAT || {
	echo "Failed to publish targets to flat cluster"
}

# How to use these scripts

These are intended to be used to sync ostree branches from feeds.toradex.com and then publish them to an ota server.

`syncOstreeFeeds.sh` will pull the latest refs from feeds.toradex.com for all known ostree branches
- This script requires an initialized ostree_repo that is setup by these commands  
  `ostree init --repo=./ostree_repo --mode=archive`  
  `ostree --repo=./ostree_repo remote add --no-gpg-verify toradex-ostree http://feeds1.toradex.com/ostree`

`pushAllToCluster.sh` will push the latest refs from local ostree_repo to an ota cluster

`syncReposAndPush.sh` is a convenience function that will call both above scripts to first sync, and then publish.



# Implementation Details

## common.sh
`common.sh` contains a useful series of loops that generate all permutations of the various ostree branches we support in Torizon.
It uses diction defined in this confluence doc: https://toradex.atlassian.net/wiki/spaces/TOR/pages/357367857/OSTree+branch+naming

The `PrintPackages` function is a good template copy and manipulate to perform operations on all packages in Torizon.

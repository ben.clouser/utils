#!/usr/bin/env bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

CLUSTER_BASE_URL=$1 
JWT_TOKEN=$2
ONLY_PURPOSE=$3



function usage {
	echo "./pushAllToCluster.sh <cluster-base-url> <super-user-jwt-token> [build-purpose(nightly|monthly|release)]"
	exit 0
}

if [ -z $CLUSTER_BASE_URL ]; then
	echo "must specify cluster base url. As argument 1 Example: app-int.torizon.io"
	exit -2
fi

if [ -z $JWT_TOKEN ]; then
    echo "must specify JWT Token as argument 2"
    exit -2
fi


# Retrieve credentials.zip for default user
http_code=$(curl -s -w '%{http_code}' --max-time 30 -H "Authorization: Bearer $JWT_TOKEN" https://${CLUSTER_BASE_URL}/api/accounts/users/00000000-0000-0000-0000-000000000000/keys/credentials.zip -o default-credentials.zip)
if [[ ! $http_code -eq 200 ]]; then
  # if we failed, device.zip will be a text file with the errors
  echo "Failed to download default credentials.zip"
  echo "HTTP Response-Code: ${http_code}"
  echo "Response-Body: $( cat default-credentials.zip)"
  exit -1
fi


source $SCRIPT_DIR/common.sh

# If a specific build purpose is set. Override list of all Build Purposes
if [ -n "${ONLY_PURPOSE}" ]; then
	if [[ " ${BUILD_PURPOSES[*]} " == *" $ONLY_PURPOSE "* ]]; then
		echo "NOTE: Only publishing images with build_purpose: $ONLY_PURPOSE"
		echo ""
		BUILD_PURPOSES=("$ONLY_PURPOSE")
	else
		echo "unknown build purpose specified at argument 3, \"$ONLY_PURPOSE\". Should be one of:"
		echo "${BUILD_PURPOSES[@]}"
		exit -1
	fi
fi

publishedPkgs=0
numSkippedPkgs=0
failedPkgs=()

for machine in ${MACHINES[@]}
do
	echo "== Starting OSTree upload for $machine"
	distros=( $(GetDistros $machine) )
	if [ -z "$distros" ]; then
		echo ""
		echo "WARNING WARNING WARNING!!!"
		echo "No distros found for $machine!!! SKIPPING"
		echo ""
		numSkippedPkgs=$((numSkippedPkgs + 1))
		continue
	fi
	for distro in ${distros[@]}
	do
		for image in ${IMAGES[@]}
		do
			for buildPurpose in ${BUILD_PURPOSES[@]}
			do
				echo "- ${OE_VERSION}/${machine}/${distro}/${image}/${buildPurpose}"
				OSTREE_BRANCH="${OE_VERSION}/${machine}/${distro}/${image}/${buildPurpose}"
				docker run -it -v /deploy -v $(pwd):/workdir -v storage:/storage --net=host -v /var/run/docker.sock:/var/run/docker.sock torizon/torizoncore-builder:2 push $OSTREE_BRANCH --repo ./ostree_repo --hardwareid $machine --credentials ./default-credentials.zip || {
					echo "WARNING: Failed to publish $OSTREE_BRANCH. Skipping"
					failedPkgs+=("$OSTREE_BRANCH")
					numSkippedPkgs=$((numSkippedPkgs + 1))
				}
				publishedPkgs=$((publishedPkgs + 1))
			done
		done
	done
	echo ""
done
	
echo "Done Pushing ostree packages: Published ${publishedPkgs} and skipped (or failed) ${numSkippedPkgs}"


echo ""
echo "Branches that failed:"
for branch in ${failedPkgs[@]}
do
echo "${branch}"
done

echo ""
echo "== Syncing default targets metadata to delegated metadata ... "
curl -H "Authorization: Bearer ${JWT_TOKEN}" -X GET https://${CLUSTER_BASE_URL}/api/metadata/delegations/upstream-sync || {
	echo "Failed to sync default targets.json with the delegated metadata files"
	exit -1
}
echo "Success"

#!/usr/bin/env bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"

SERVER_DIR="${SCRIPT_DIR}/../generated/ota-ce.toradex.int"
DEVICE_IP=${DEVICE_IP:-"10.12.1.36"}
DEVICE_USER=${DEVICE_USER:-"torizon"}

### not always necessary
sudo rm ~/.ssh/known_hosts
sudo rm /root/.ssh/known_hosts

# All of these will end up in /var/sota/ on the device (dont have root perms to push directly)
scp ${SERVER_DIR}/server_ca.pem ${DEVICE_USER}@${DEVICE_IP}:./root.crt
scp ${SERVER_DIR}/devices/*-*-*-*-*/client.pem ${DEVICE_USER}@${DEVICE_IP}:./
sudo scp ${SERVER_DIR}/devices/*-*-*-*-*/pkey.pem ${DEVICE_USER}@${DEVICE_IP}:./


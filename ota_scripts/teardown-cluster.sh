#!/usr/bin/env bash

SCRIPT_DIR="$(dirname $(readlink -f $0))"

## Get the NFS server information before blowing everything away
# All the OTA Persistent Volumes share the same directory. So we can just look at one of them
NFS_SERVER=$(kubectl get pv --selector=app="ota" --output jsonpath='{.items[0].spec.nfs.server}' 2> /dev/null) || NFS_SERVER=""
NFS_PATH=$(kubectl get pv --selector=app="ota" --output jsonpath='{.items[0].spec.nfs.path}' 2> /dev/null) || NFS_PATH=""


## Manually delete the PVC
echo "==== Deleting Persistent Volume Claims..."
pvc=$(kubectl get pvc --selector=app="kafka" --output jsonpath='{.items[0].metadata.name}' 2>  /dev/null) && echo "Deleting kafka pvc" && sudo kubectl delete pvc $pvc &
pvc=$(kubectl get pvc --selector=app="mysql" --output jsonpath='{.items[0].metadata.name}' 2> /dev/null) && echo "Deleting mysql pvc" && sudo kubectl delete pvc $pvc &
pvc=$(kubectl get pvc --selector=app="treehub" --output jsonpath='{.items[0].metadata.name}' 2> /dev/null) && echo "Deleting treehub pvc" && sudo kubectl delete pvc $pvc &
pvc=$(kubectl get pvc --selector=app="zookeeper" --output jsonpath='{.items[0].metadata.name}' 2> /dev/null) && echo "Deleting zookeeper pvc" && sudo kubectl delete pvc $pvc &

sleep 2; # Give pvc just a smidge of time to delete... found it to prevent hangs

echo "==== Deleting services..."
sudo kubectl delete -f ${SCRIPT_DIR}/../services.yaml

echo "==== Deleting infra..."
sudo kubectl delete -f ${SCRIPT_DIR}/../infra.yaml

echo "==== Deleting secrets..."
kubectl delete secret tuf-keyserver-encryption 
kubectl delete secret user-keys
kubectl delete secret gateway-tls
## Cleanup all the generated stuff
sudo rm -rf ${SCRIPT_DIR}/../generated

echo "=== Deleting artifact files on the NFS Mount"
if [ -z "$NFS_SERVER" ] || [ -z "$NFS_PATH" ]; then
    echo "Invalid NFS server or path"
    exit -1;
fi

MOUNT_DIR=$(mktemp -d)
sudo mount ${NFS_SERVER}:${NFS_PATH} ${MOUNT_DIR} || {
    echo "Failed to mount nfs shared drive"
    exit -1
}

sudo rm -r ${MOUNT_DIR}/*

sudo umount ${MOUNT_DIR} && sudo rm -r ${MOUNT_DIR}


#!/usr/bin/env bash

PROVISIONING_TOKEN=$1
#AUTOPROV_URL=https://app.dev.torizon.io/api/accounts/devices
AUTOPROV_URL=https://app.torizon.io/api/accounts/devices
#AUTOPROV_URL=https://app.pilot.torizon.io/api/accounts/devices
DEVICE_ID=apalis-imx6

http_code=$(curl -s -w '%{http_code}' --max-time 30 -X POST -H "Authorization: Bearer $PROVISIONING_TOKEN" "$AUTOPROV_URL" -d "{\"device_id\": \"${DEVICE_ID}\", \"device_name\": \"${DEVICE_NAME}\"}" -o device.zip
)

if [[ ! $http_code -eq 200 ]]; then
    # if we failed, device.zip will be a text file with the errors
    echo "Failed to download token :("
    echo "HTTP ERROR ${http_code}"
    cat device.zip
    exit 1
fi

#!/usr/bin/env bash

garage-sign init --credentials credentials.zip --repo test-repo

garage-sign targets pull --repo test-repo

garage-sign targets upload --repo test-repo --input ./docker-compose.yml --name bens-hello-dc --version 3.0

garage-sign targets add-uploaded --repo test-repo --input ./docker-compose.yml --name bens-hello-dc --version 3.0 --hardwareids docker-compose

garage-sign targets sign --repo test-repo --key-name targets

garage-sign targets push --repo test-repo







#!/usr/bin/env bash


ACCESS_TOKEN=$JWT_TOKEN
HOST=https://app-local.int.toradex.com
#HOST=https://app-dev2.int.toradex.com
#HOST=https://app.pilot.torizon.io
#HOST=https://app.torizon.io

curl -H "Authorization: Bearer $ACCESS_TOKEN" -H "content-type: application/json" ${HOST}/api/accounts/token  | jq -r .
 

#curl -H "Authorization: Bearer $ACCESS_TOKEN" https://app.torizon.io/api/accounts/users/clients | jq -r .


#!/usr/bin/env bash

DEVICE_GW_HOST="https://dgw.dev.torizon.io:8443"

curl --verbose $DEVICE_GW_HOST/monitoring/fluentbit-metrics -H "content-type:application/json" -X POST --cacert ./deviceZip/root.crt --cert ./deviceZip/chain.pem -d @./device-metrics.json

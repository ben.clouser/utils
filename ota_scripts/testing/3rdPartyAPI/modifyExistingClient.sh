#!/usr/bin/env bash


ACCESS_TOKEN=$JWT_TOKEN
HOST=https://app-local.int.toradex.com
#HOST=https://app.torizon.io

CLIENT_ID=$(curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients | jq -r '.[0].client_id')


curl -H "Authorization: Bearer $ACCESS_TOKEN" -H "content-type: application/json" -X PATCH ${HOST}/api/accounts/users/clients/${CLIENT_ID} -d @patch_name.json

curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients | jq -r .



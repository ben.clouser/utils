#!/usr/bin/env bash


API_ID=$1
ACCESS_TOKEN=$JWT_TOKEN

curl -H "Authorization: Bearer $ACCESS_TOKEN" -X DELETE https://app-local.int.toradex.com/api/accounts/users/clients/$API_ID
 

curl -H "Authorization: Bearer $ACCESS_TOKEN" https://app-local.int.toradex.com/api/accounts/users/clients | jq -r .


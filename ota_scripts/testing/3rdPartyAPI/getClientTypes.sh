#!/usr/bin/env bash


ACCESS_TOKEN=$JWT_TOKEN
HOST=https://app-local.int.toradex.com
#HOST=https://app.torizon.io

curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/client-types | jq -r .

echo ""
echo ""


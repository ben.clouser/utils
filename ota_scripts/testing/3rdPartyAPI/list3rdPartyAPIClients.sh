#!/usr/bin/env bash


ACCESS_TOKEN=$JWT_TOKEN
HOST=https://app-local.int.toradex.com
#HOST=https://app.torizon.io

curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients | jq -r .

echo ""
echo ""

echo "And the details for the first apiclient:"
CLIENT_ID=$(curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients | jq -r '.[0].client_id')
echo "clientID: ${CLIENT_ID}"



curl -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients/${CLIENT_ID} | jq -r .
#curl -X DELETE -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/users/clients/${CLIENT_ID} | jq -r .


#!/usr/bin/env bash


CLIENT_ID=$1
CLIENT_SECRET=$2
HOST=${3:-"kc.torizon.io"}
TOKEN_URL="https://${HOST}/auth/realms/ota-users/protocol/openid-connect/token"
curl $TOKEN_URL \
 -d client_id=$CLIENT_ID \
 -d client_secret=$CLIENT_SECRET \
 -d grant_type=client_credentials | jq .





I made users ben.clouser@toradex.com and bclouse91@gmail.com manually in kc.dev2.torizon.io and added the attribute "Namespace" to their user.

ben.clouser@toradex.com (namespace: 1b7e0793-97cd-42c5-9890-277a862c8838) is the owner of the org that will be shared
bclouse91@gmail.com (namespace: 042dbd5b-5190-4732-8cc1-d175791cd2f3) is the guest user

I then manually created the entries in the `user_registry.User` table:
```
insert into User(userNamespace, providerID, userID, teamID) Values('042dbd5b-5190-4732-8cc1-d175791cd2f3','6356d2e0-6941-4188-8cd7-850ebad5ca43','6356d2e0-6941-4188-8cd7-850ebad5ca43', '');
```


# Create organization

# Add org user
```
curl --verbose -H "x-ats-namespace:1b7e0793-97cd-42c5-9890-277a862c8838" accounts/organizations/share/e92c8e83-bb7b-4135-903b-c84eba0ac109/users -H "content-type:application/json" -X POST -d @./addOrgUser
```


# Login to organization
```
curl --verbose -H "x-ats-namespace:042dbd5b-5190-4732-8cc1-d175791cd2f3" accounts/organizations/guest/e92c8e83-bb7b-4135-903b-c84eba0ac109 -H "content-type:application/json" -X POST
```

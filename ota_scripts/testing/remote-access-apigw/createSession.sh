#!/usr/bin/env bash

USER_NAMESPACE=${1:-2fdbb8a1-16eb-4a5d-aea5-90733e373b3d}
DEVICE_UUID=${2:-dd273ec4-1156-4dac-a0be-32026b85f9ed}

# Assumes you have a kubernetes proxy running
curl --verbose -H "x-ats-namespace: ${USER_NAMESPACE}" -X POST http://localhost:9001/remote-access/device/${DEVICE_UUID}/sessions -d @./create-session.json


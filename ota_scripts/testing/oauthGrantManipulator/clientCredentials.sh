#!/usr/bin/env bash

HOST=https://app.dev.torizon.io
CLIENT_ID=$1
CLIENT_SECRET=$2
TOKEN_ENDPOINT=${3:-${HOST}/api/oauth/token}

curl --verbose -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8" -X POST ${TOKEN_ENDPOINT} \
	-d "grant_type=client_credentials"  \
	-d "client_id=${CLIENT_ID}" \
	-d "client_secret=${CLIENT_SECRET}" \
	-d "scope=email"

echo ""

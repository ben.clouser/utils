#!/usr/bin/env bash

#CLIENT_ID=$(cat ./client-pilot.id)
CLIENT_ID=$(cat ./client-dev.id)
#CLIENT_SECRET=$(cat ./client-pilot.secret)
CLIENT_SECRET=$(cat ./client-dev.secret)
KC_HOST="kc.dev.torizon.io"

TOKEN=$(../3rdPartyAPI/getThirdPartyAPIToken.sh ${CLIENT_ID} ${CLIENT_SECRET} ${KC_HOST} | jq -r '.access_token')


echo "Got token of: ${TOKEN}"
HOST=https://app.torizon.io/api/v2beta
HOST=https://app.pilot.torizon.io/api/v2beta
HOST=https://app.dev.torizon.io/api/v2beta
#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/packages?limit=1000



#echo "Ok, now getting packages_external"

#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/packages_external?limit=1000

#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/packages?name=bens-sway-script&version=0.0.1&hardwareId=apalis-imx6&targetFormat=BINARY

#test-docker-compose.yml
# THis returned all of my targets... not great
#curl -w @${HOME}/curl-format.txt -X POST --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/packages?name=LdxRRyq44D&version=1&hardwareId=docker-compose&targetFormat=BINARY
curl -X POST --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/packages \
	--url-query version=1 \
	--url-query name=idkman \
	--url-query hardwareId=docker-compose \
	--url-query targetFormat=BINARY \
	-F hazzz=@./createPackage.sh

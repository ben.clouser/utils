#!/usr/bin/env bash

TOKEN_HOST="https://kc.torizon.io"
API_HOST="https://app.torizon.io"
ENV_NAME="prod"
if [ "$1" == "dev" ]; then
	TOKEN_HOST="https://kc.dev.torizon.io"
	API_HOST="https://app.dev.torizon.io"
	ENV_NAME="dev"
elif [ "$1" == "pilot" ]; then
	TOKEN_HOST="https://kc.pilot.torizon.io"
	API_HOST="https://app.pilot.torizon.io"
	ENV_NAME="dev"
fi
	

CLIENT_ID=$(cat $HOME/.apiCredentials/client-${ENV_NAME}.id)
CLIENT_SECRET=$(cat $HOME/.apiCredentials/client-${ENV_NAME}.secret)


ACCESS_TOKEN=$(curl -s ${TOKEN_HOST}/auth/realms/ota-users/protocol/openid-connect/token \
 -d client_id=$CLIENT_ID \
 -d client_secret=$CLIENT_SECRET \
 -d grant_type=client_credentials | jq -r .access_token)

echo "CLIENT_ID: $CLIENT_ID"
echo "CLIENT_SECRET: $CLIENT_SECRET"
echo "TOKEN_HOST: ${TOKEN_HOST}"
echo "ACCESS_TOKEN: ${ACCESS_TOKEN}"

echo ""
echo ""
echo "-------- Devices -------"
curl --verbose -H "Authorization: Bearer $ACCESS_TOKEN" ${API_HOST}/api/v2beta/non-existent-endpoint

if [ $? -ne 0 ]; then
	echo "Failed to get devices"
	exit 1
fi



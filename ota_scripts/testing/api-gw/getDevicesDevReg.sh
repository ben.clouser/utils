#!/usr/bin/env bash

ACCESS_TOKEN=$1
#curl --verbose =H "x-ats-namespace: default" http://127.0.0.1:8001/api/v1/namespaces/default/services/device-registry:80/proxy/api/v1/devices

curl --verbose -H "Authorization: Bearer $ACCESS_TOKEN" https://app.dev.torizon.io/api/v1/devices

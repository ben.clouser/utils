#!/usr/bin/env bash

FLEET_ID=$1
CLIENT_ID=$(cat ~/.apiCredentials/client-pilot.id)
CLIENT_SECRET=$(cat ~/.apiCredentials/client-pilot.secret)
KC_HOST="kc.pilot.torizon.io"
TOKEN=$(../3rdPartyAPI/getThirdPartyAPIToken.sh ${CLIENT_ID} ${CLIENT_SECRET} ${KC_HOST} | jq -r '.access_token')

HOST=https://app.pilot.torizon.io
curl -q -H "Authorization: Bearer ${TOKEN}" ${HOST}/api/v2beta/fleets/${FLEET_ID}/devices | jq .

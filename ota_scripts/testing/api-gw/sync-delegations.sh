JWT_TOKEN=$1

HOST="https://app.pilot.torizon.io"

DELEGATIONS=("nightly" "monthly" "quarterly" "lts" "containers")

for str in ${DELEGATIONS[@]}; do
# Fetch the list of images for the given delegation
echo "Fetching images for tdx-$str"
    curl "$HOST/api/metadata/delegations/tdx/tdx-$str.json" \
    -H "Authorization: Bearer $JWT_TOKEN" -o target.json

# Push the images to the to the user specific repository
    echo "Pushing images for  tdx-$str to the user specific repository"

    curl "$HOST/api/v1/user_repo/delegations/tdx-$str.json" \
    -X 'PUT' \
    -H 'Content-Type: application/json;charset=UTF-8' \
    -H "Authorization: Bearer $JWT_TOKEN"\
    -d @target.json
done

#!/usr/bin/env bash


CLIENT_ID=$(cat ./client.id)
CLIENT_SECRET=$(cat ./client.secret)
HOST_PREFIX=""


if [ "$1" = "pilot" ]; then
	CLIENT_ID=$(cat ./client-pilot.id)
	CLIENT_SECRET=$(cat ./client-pilot.secret)
	HOST_PREFIX=".pilot"
elif [ "$1" = "dev" ]; then
	CLIENT_ID=$(cat ./client-dev.id)
	CLIENT_SECRET=$(cat ./client-dev.secret)
	HOST_PREFIX=".dev"
elif [ "$1" != "" ]; then
	echo "No matching env for $1"
	exit -1
fi


KC_HOST="kc${HOST_PREFIX}.torizon.io"
echo "Keycloak host is: $KC_HOST" 
TOKEN=$(../3rdPartyAPI/getThirdPartyAPIToken.sh ${CLIENT_ID} ${CLIENT_SECRET} ${KC_HOST} | jq -r '.access_token')

echo "Got token of: ${TOKEN}"

HOST="https://app${HOST_PREFIX}.torizon.io/api/v2beta"
#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/devices/packages?limit=1000
#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/devices/packages?limit=1000
curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" "${HOST}/devices/network"


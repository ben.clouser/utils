#!/usr/bin/env bash

CLIENT_ID=$(cat ~/.apiCredentials/client-pilot.id)
CLIENT_SECRET=$(cat ~/.apiCredentials/client-pilot.secret)
KC_HOST="kc.pilot.torizon.io"
TOKEN=$(../3rdPartyAPI/getThirdPartyAPIToken.sh ${CLIENT_ID} ${CLIENT_SECRET} ${KC_HOST} | jq -r '.access_token')

echo "BEN SAYS: Here is access token: "
echo $TOKEN
echo ""

HOST=https://app.pilot.torizon.io/api/v2beta
curl --verbose -H "Authorization: Bearer ${TOKEN}" -X PATCH -d @./cancelUpdate.json ${HOST}/updates

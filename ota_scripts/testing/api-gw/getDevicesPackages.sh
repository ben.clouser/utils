#!/usr/bin/env bash


CLIENT_ID=$(cat $HOME/.apiCredentials/client-senceive.id)
CLIENT_SECRET=$(cat $HOME/.apiCredentials/client-senceive.secret)
HOST_PREFIX=""


if [ "$1" = "pilot" ]; then
	CLIENT_ID=$(cat ./client-pilot.id)
	CLIENT_SECRET=$(cat ./client-pilot.secret)
	HOST_PREFIX=".pilot"
elif [ "$1" = "dev" ]; then
	CLIENT_ID=$(cat ./client-dev.id)
	CLIENT_SECRET=$(cat ./client-dev.secret)
	HOST_PREFIX=".dev"
elif [ "$1" != "" ]; then
	echo "No matching env for $1"
	exit -1
fi


DEVICES=( "119a4680-e5a0-48ce-8e6e-b47035236877" \
        "a08c3e11-13bf-47e0-b702-304000423080" \
        "64fc9d36-aaf7-420d-a739-59e49736724d" \
        "163ad30c-8c08-47c9-9000-50cbd57fa516" \ 
        "a3bcc82b-4646-4d4f-8c1a-be80c9731c3f" \ 
        "4839d56e-3f8e-48dc-b59b-90037cfab7fd" \
        "64780467-6474-478d-9995-e3fc3d38f06f" \
        "61d6ec4d-8b9c-4353-ad39-a8490dc89669" \
        "615ebdc4-c592-44cf-99fa-e0b7f29556e5" \
        "80d77332-74bb-4a5c-b787-cc2d51951d67" \ 
        "f1b8ecbb-c765-415a-9d37-9ffdc3d1bdfe" \
        "3d18f024-82ed-460a-af1f-35146274cc07" \
        "c50b22bc-144b-420d-9878-5796cd3a3d68" \
        "eedde6e2-9aa4-4609-83b2-17e721d82e9d" \ 
        "bf4660b8-dee0-448d-8a33-8a77fb551f6d" \
        "76e6a530-3331-4800-9362-f09ab5747aa6" \ 
        "3195c178-8867-4f51-a1b7-fac449d56625" \
        "527c2244-317c-4b42-b689-ae5ae2899553" \
        "fdcb8d77-75c5-4777-b760-0c71daa63c56" )






KC_HOST="kc${HOST_PREFIX}.torizon.io"
echo "Keycloak host is: $KC_HOST" 
TOKEN=$(../3rdPartyAPI/getThirdPartyAPIToken.sh ${CLIENT_ID} ${CLIENT_SECRET} ${KC_HOST} | jq -r '.access_token')

echo "Got token of: ${TOKEN}"

HOST="https://app${HOST_PREFIX}.torizon.io/api/v2beta"
#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/devices/packages?limit=1000
#curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" ${HOST}/devices/packages?limit=1000

# curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" "${HOST}/devices/packages?deviceUuid=119a4680-e5a0-48ce-8e6e-b47035236877&deviceUuid=a08c3e11-13bf-47e0-b702-304000423080&deviceUuid=64fc9d36-aaf7-420d-a739-59e49736724d&deviceUuid=163ad30c-8c08-47c9-9000-50cbd57fa516&deviceUuid=a3bcc82b-4646-4d4f-8c1a-be80c9731c3f&deviceUuid=4839d56e-3f8e-48dc-b59b-90037cfab7fd&deviceUuid=64780467-6474-478d-9995-e3fc3d38f06f&deviceUuid=61d6ec4d-8b9c-4353-ad39-a8490dc89669&deviceUuid=615ebdc4-c592-44cf-99fa-e0b7f29556e5&deviceUuid=80d77332-74bb-4a5c-b787-cc2d51951d67&deviceUuid=f1b8ecbb-c765-415a-9d37-9ffdc3d1bdfe&deviceUuid=3d18f024-82ed-460a-af1f-35146274cc07&deviceUuid=c50b22bc-144b-420d-9878-5796cd3a3d68&deviceUuid=eedde6e2-9aa4-4609-83b2-17e721d82e9d&deviceUuid=bf4660b8-dee0-448d-8a33-8a77fb551f6d&deviceUuid=76e6a530-3331-4800-9362-f09ab5747aa6&deviceUuid=3195c178-8867-4f51-a1b7-fac449d56625&deviceUuid=527c2244-317c-4b42-b689-ae5ae2899553&deviceUuid=fdcb8d77-75c5-4777-b760-0c71daa63c56"
curl -w @${HOME}/curl-format.txt --verbose -H "Authorization: Bearer $TOKEN" "${HOST}/devices/packages?deviceUuid=119a4680-e5a0-48ce-8e6e-b47035236877"

echo "Ok all done"


#!/usr/bin/env bash

ENRICHED_TOKEN=$1
HOST=app-dev2.int.toradex.com
curl -H "Authorization: Bearer $ENRICHED_TOKEN" https://${HOST}/api/accounts/users -X POST

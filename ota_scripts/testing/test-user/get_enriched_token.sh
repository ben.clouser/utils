#!/usr/bin/env bash

HOST=app.dev.torizon.io

# Some needed variables...
UUID="167d7cda-2072-11ea-8733-5f9850348ac9"
JWT_UTIL_PRIV_KEY_PATH=$VAULT_PRIVATE_KEY_PATH
JWT_UTIL_PUB_KEY_PATH=$VAULT_PUBLIC_KEY_PATH

#JWT_UTIL_PRIV_KEY_PATH="$HOME/.tptf/kong-credentials/upstream-sync/private.pem"
#JWT_UTIL_PUB_KEY_PATH="$HOME/.tptf/kong-credentials/upstream-sync/public.pem"

# create token with jwt-util
#JWT_UTIL_TOKEN=$(jwt-util --quiet -iss api-testing-pub-key -namespace not-real-namespace -providerID ${UUID} -priv ${JWT_UTIL_PRIV_KEY_PATH} -pub ${JWT_UTIL_PUB_KEY_PATH})
JWT_UTIL_TOKEN=$(jwt-util -iss api-testing-pub-key -namespace not-real-namespace -providerID ${UUID} -priv ${JWT_UTIL_PRIV_KEY_PATH} -pub ${JWT_UTIL_PUB_KEY_PATH})


#echo "PUB: $(cat $JWT_UTIL_PUB_KEY_PATH)"
#echo "PRIV: $(cat $JWT_UTIL_PRIV_KEY_PATH)"

curl --verbose -H "Authorization: Bearer $JWT_UTIL_TOKEN" -H "Content-Type: Application/json" https://${HOST}/api/accounts/users/test-user/enable -d @./test-user-enable.json



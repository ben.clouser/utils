#!/usr/bin/env bash

for i in {0..19}
do
	echo "Disabling user: otatestuser+$((i+1))@toradex.com"
	./disableTestUser.sh otatestuser+$((i + 1))@toradex.com
done

for i in {0..4}
do
	echo "Disabling user: otatestuser_org+$((i+1))@toradex.com"
	./disableTestUser.sh otatestuser_org+$((i + 1))@toradex.com
done

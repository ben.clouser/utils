#!/usr/bin/env bash

HOST=app.torizon.io

# Some needed variables...
JWT_UTIL_PRIV_KEY_PATH="$HOME/.tptf/kong-credentials/upstream-sync/private.pem"
JWT_UTIL_PUB_KEY_PATH="$HOME/.tptf/kong-credentials/upstream-sync/public.pem"

#echo "Priv: $(cat $JWT_UTIL_PRIV_KEY_PATH)"
#echo "Pub: $(cat $JWT_UTIL_PUB_KEY_PATH)"

# create token with jwt-util
JWT_UTIL_TOKEN=$(jwt-util -iss "ci-upstream-sync-pub-key" -namespace "78ac2c58-efd3-11eb-8d02-9b402d158af4" -providerID "11111111-1111-1111-1111-111111111111" -priv ${JWT_UTIL_PRIV_KEY_PATH} -pub ${JWT_UTIL_PUB_KEY_PATH})
echo ${JWT_UTIL_TOKEN}
#https://app.torizon.io/api/metadata/delegations/upstream-sync
#curl --verbose -H "Authorization: Bearer $JWT_UTIL_TOKEN" -H "Content-Type: Application/json" https://${HOST}/api/metadata/delegations/upstream-sync



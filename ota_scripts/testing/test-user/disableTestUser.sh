#!/usr/bin/env bash

HOST=app.pilot.torizon.io
HOST=app.dev.torizon.io

# Some needed variables...
UUID="167d7cda-2072-11ea-8733-5f9850348ac9"
JWT_UTIL_PRIV_KEY_PATH=$VAULT_PRIVATE_KEY_PATH
JWT_UTIL_PUB_KEY_PATH=$VAULT_PUBLIC_KEY_PATH

# create token with jwt-util
JWT_UTIL_TOKEN=$(jwt-util --quiet -iss api-testing-pub-key -namespace not-real-namespace -providerID ${UUID} -priv ${JWT_UTIL_PRIV_KEY_PATH} -pub ${JWT_UTIL_PUB_KEY_PATH})

if [ -z "$JWT_UTIL_TOKEN" ]; then
	echo "JWT_UTIL_TOKEN creation failed!"
	exit -1
fi

echo "BEN SAYS: JWT_UTIL_TOKEN: ${JWT_UTIL_TOKEN}"
#echo "JWT_UTIL TOKEN:"
#echo $JWT_UTIL_TOKEN

TEST_USER_EMAIL=$1

curl --verbose -H "Authorization: Bearer $JWT_UTIL_TOKEN" -H "Content-Type: Application/json" https://${HOST}/api/accounts/users/test-user/disable --data '{"email":"'${TEST_USER_EMAIL}'"}'


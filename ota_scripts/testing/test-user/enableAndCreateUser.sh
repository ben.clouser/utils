#!/usr/bin/env bash

HOST=app-dev2.int.toradex.com
ENRICHED_TOKEN=$(./get_enriched_token.sh | jq -r .access)

curl -H "Authorization: Bearer $ENRICHED_TOKEN" https://${HOST}/api/accounts/users -X POST



echo $ENRICHED_TOKEN > enriched_token.json

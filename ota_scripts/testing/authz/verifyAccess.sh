#!/usr/bin/env bash


# get access token for user
ACCESS_TOKEN=$1
HOST=${2:-"kc.torizon.io"}
REALM="ota-users"
RESOURCE_SERVER_CLIENT_ID="ota-users-resource-server"

if [ -z $ACCESS_TOKEN ];then
	echo "Specify access token as argument 1"
	exit -1
fi

#curl --verbose -H "Authorization: Bearer ${ACCESS_TOKEN}" -X POST "https://${HOST}/auth/realms/ota-users/protocol/openid-connect/token?grant_type=urn:ietf:params:oauth:grant-type:uma-ticket&audience=ota-users-resource-server&permission=utils#create&response_mode=decision"


#curl --verbose "https://${HOST}/auth/realms/${REALM}/protocol/openid-connect/userinfo" \
#	-H "Authorization: Bearer ${ACCESS_TOKEN}" -X POST 


curl --verbose -X POST \
  https://${HOST}/auth/realms/${REALM}/protocol/openid-connect/token \
  -H "Content-Type: Application/x-www-form-urlencoded" \
  -H "Authorization: Bearer ${ACCESS_TOKEN}" \
  --data "grant_type=urn:ietf:params:oauth:grant-type:uma-ticket" \
  --data "audience=${RESOURCE_SERVER_CLIENT_ID}" \
  --data "response_mode=decision" 



#!/usr/bin/env bash

CLIENT_ID=$1
USERNAME=$2
PASSWORD=$3
HOST=https://kc.dev.torizon.io
#HOST=https://kc-pilot.torizon.io

curl --verbose -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8" -X POST ${HOST}/auth/realms/ota-users/protocol/openid-connect/token \
	-d "grant_type=password"  \
	-d "client_id=${CLIENT_ID}" \
	-d "username=${USERNAME}" \
	-d "password=${PASSWORD}" \



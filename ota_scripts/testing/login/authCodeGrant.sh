#!/usr/bin/env bash

HOST=kc.dev.torizon.io
CLIENT_ID=${1:-"ota-app"}


#curl --verbose -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8" "https://${HOST}/auth/realms/ota-users/protocol/openid-connect/auth?response_type=code&client_id=${CLIENT_ID}&redirect_uri=https://app.dev.torizon.io/api/accounts/authcode&state=0htoo2n3bhpjprbn0ua2k"

REDIRECT_URI="https://app.dev.torizon.io/api/accounts/authcode"


echo "hit this in your browser: https://${HOST}/auth/realms/ota-users/protocol/openid-connect/auth?response_type=code&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&state=0htoo2n3bhpjprbn0ua2k"


echo -n "Enter code: " 
read CODE

curl --location --request POST "https://${HOST}/auth/realms/ota-users/protocol/openid-connect/token" \
--header "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "grant_type=authorization_code" \
--data-urlencode "client_id=${CLIENT_ID}" \
--data-urlencode "code=${CODE}" \
--data-urlencode "redirect_uri=${REDIRECT_URI}"

#!/usr/bin/env bash

TOTP=$1
HOST=https://kc.dev.int.toradex.com
HOST=https://kc.pilot.torizon.io

curl --verbose -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8" -X POST ${HOST}/auth/realms/ota-users/protocol/openid-connect/token \
	-d "grant_type=password"  \
	-d "client_id=ota-app" \
	-d "username=ben.clouser@toradex.com" \
	-d "password=S3micolon!" \
	-d "scope=openid email profile" \
	-d "totp=$TOTP"



#!/usr/bin/env bash

HOST=https://kc.dev.torizon.io

USERNAME=$1
PASSWORD=$2

curl --verbose -H "Content-Type: application/x-www-form-urlencoded;charset=utf-8" -X POST ${HOST}/auth/realms/ota-users/protocol/openid-connect/token \
	-d "grant_type=password"  \
	-d "client_id=bens-single-api-client" \
	-d "client_secret=oeH2ofICK5GiMoPRVDaestNfOBrzdWcW" \
	-d "username=${USERNAME}" \
	-d "password=${PASSWORD}" 



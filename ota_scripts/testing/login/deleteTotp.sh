#!/usr/bin/env bash


#HOST=https://app.torizon.io
HOST=https://app-pilot.torizon.io
HOST=https://app-local.int.toradex.com

ID=$1
curl --verbose -H "Authorization: Bearer $JWT_TOKEN" ${HOST}/api/accounts/users/totp/${ID} -X DELETE -d @./delete-totp.json -H "content-type: application/json" | jq -r .


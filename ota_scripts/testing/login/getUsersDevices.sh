#!/usr/bin/env bash


TOKEN=$(cat ./access.token)

curl --verbose -H "Authorization: Bearer ${TOKEN}" "https://app.dev.torizon.io/api/v1/devices"

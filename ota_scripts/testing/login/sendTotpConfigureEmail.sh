#!/usr/bin/env bash


HOST=https://app-local.int.toradex.com
HOST=https://app-pilot.torizon.io
#HOST=https://app.torizon.io

 
curl --verbose -H "Authorization: Bearer $JWT_TOKEN" ${HOST}/api/accounts/users/totp -X POST -H "content-type: application/json" | jq -r .


#!/usr/bin/env bash


ACCESS_TOKEN=$JWT_TOKEN
HOST=https://app-local.int.toradex.com
#HOST=https://app.pilot.torizon.io
#HOST=https://app.torizon.io
ORG_ID=$1

curl -X POST -H "Authorization: Bearer $ACCESS_TOKEN" ${HOST}/api/accounts/organizations/guest/${ORG_ID} | jq -r .
 

#curl -H "Authorization: Bearer $ACCESS_TOKEN" https://app.torizon.io/api/accounts/users/clients | jq -r .


#!/usr/bin/env bash

CLUSTER=$1

if [ -z "$CLUSTER" ]; then
	echo "Please specify the cluster as argument 1"
	exit -1
fi

JWT_TOKEN=${2:-${JWT_TOKEN}}

if [ -z "$JWT_TOKEN" ]; then
	echo "JWT_TOKEN Should be set in order to pull credentials"
	exit -1
fi


MODULES=( apalis-imx6 apalis-imx8 colibri-imx7 verdin-imx8mm )
BUILD_TYPES=( torizon/torizon-core-docker torizon/torizon-core-podman torizon-rt/torizon-core-docker-rt torizon-rt/torizon-core-podman-rt )


RAND_DIR=$(mktemp -d)


pushd $RAND_DIR


# First get credentials
curl --insecure -H "Authorization: Bearer $JWT_TOKEN" https://app-${CLUSTER}.int.toradex.com/api/credentials/credentials.zip -o credentials.zip

for module in ${MODULES[@]}
do
	for buildType in ${BUILD_TYPES[@]}
	do
		echo "=== Pulling and publishing $module/${buildType} ==="
		echo ""
		docker run --network=host --rm -v ${PWD}:/data torizon/ostree-push-to-garage pushtogarage -v ${module} http://feeds1.toradex.com/ostree/torizon-master/nightly/${module}/ ${module}/${buildType} /data/credentials.zip
	done
done

popd


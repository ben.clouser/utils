#!/usr/bin/env bash



ACCESS_TOKEN=$1

if [ -z $ACCESS_TOKEN ]; then
	echo "Acess token needs to be specified as argument 1"
	exit 0
fi


HOST=https://app.pilot.torizon.io
HOST=https://app.torizon.io


PROVISION_OBJECT=$( curl $HOST/api/provision-code )

PROVISION_UUID=$(echo $PROVISION_OBJECT | jq -r .provisionUuid)
PROVISION_CODE=$(echo $PROVISION_OBJECT | jq -r .provisionCode)

if [ -z $PROVISION_UUID ]; then
	echo "Failed to get provision uuid"
	exit 0
fi
if [ -z $PROVISION_CODE ]; then
	echo "Failed to get provision CODE"
	exit 0
fi


echo "Provision UUID is: ${PROVISION_UUID}"
echo "Provision Code is: ${PROVISION_CODE}"

echo ""

echo "Claiming it for user with specified access token..."
echo "{\"provisionUuid\":\"${PROVISION_UUID}\"}"
curl -H "content-type: application/json" -H "Authorization: Bearer $ACCESS_TOKEN" -X POST $HOST/api/accounts/prov-claim -d "{\"provisionCode\":\"${PROVISION_CODE}\"}" || {
	echo "Failed to claim provision code"
	exit 0
}
echo ""
echo ""
echo "Retrieve access token as device..."
curl $HOST/api/provision-code?provisionUuid=$PROVISION_UUID | jq .

echo ""

### Using vpn script
Our aws cluster has been whitelisted which means you need to add routes to your computer after connected to the vpn.
I made a hook script for adding these automatically.
If you create the directories and put this script into /etc/vpnc/post-connect.d/post-vpn.sh and make it executable
the correct routes should be added everytime you connect using openconnect. I tested on linux and macOS.

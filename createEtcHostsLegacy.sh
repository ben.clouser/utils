#!/usr/bin/env bash 


OUTPUT_FILE_NAME="etc.hosts"
SVC_NAMES=(ota-tdx-ambassador app reverse-proxy gateway-service)
DNS_NAMES=("app.api.toradex.int" "app.toradex.int" \
    "api.toradex.int oauth2.toradex.int web-events.toradex.int" \
    "ota-ce.toradex.int" )

echo > $OUTPUT_FILE_NAME

for i in "${!SVC_NAMES[@]}"; do
    name=${SVC_NAMES[$i]}
    echo "${name}"
    SVC_STR=$(kubectl get svc |grep ${name} |grep LoadBalancer)
    SVC_ARR=($SVC_STR)
    echo "${SVC_ARR[3]} ${DNS_NAMES[$i]}" >> $OUTPUT_FILE_NAME
done

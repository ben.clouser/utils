#!/usr/bin/env bash

JWT_TOKEN=$1
HOST="https://app.torizon.io"

echo "access token is: $JWT_TOKEN"
#curl --verbose -H "Authorization: Bearer $JWT_TOKEN" https://app-dev2.int.toradex.com/api/accounts/leads -d @./accounts-lead.json -H "Content-Type: Application/json"
curl -w '@${HOME}/curl-format.txt' --verbose -H "Authorization: Bearer $JWT_TOKEN" $HOST/api/accounts/leads -d @./accounts-lead.json -H "Content-Type: Application/json"

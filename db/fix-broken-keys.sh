#!/usr/bin/env bash



# Got list from querying `select * from keyserver.key_gen_requests where status="ERROR";`
REPO_IDS=( "46935318-5ee0-4df8-b1bd-b807a43b2e0d" 
"7c850828-0750-4955-9a34-c587eb0adc17" 
"282fa520-5fe4-4125-9b72-6b847f16efcd" 
"75d98f18-1ccf-4f79-9a08-03d26e0bb56a" 
"f49fb8d0-e023-4abb-820c-383abaf51baa" 
"85949c02-bee2-474e-b1b5-e5e991b052a9" 
"64ecbe69-05a8-4287-b7ee-986ba25e89c2" 
"7ee73957-c575-4921-829b-e16ef99f070d" 
"2c038c6a-7296-4cc0-ad98-952740b97e4c" 
"534c4fc9-11cd-4033-894f-8b11132e0452" 
"7ad5b5c7-c483-418d-b654-bcfebcf0f597" 
"8f2440d5-6255-4863-bca1-3d9b260d6494" 
"657e5622-201a-480f-a488-fd11690ae7e9" 
"f85eeb03-54d6-479f-b258-998f0cd20e65" )



for repoID in ${REPO_IDS[@]}; do
    output=$(mysql -proot -h mariadb -u root --port 3306 -e "select * from director.repo_names where repo_id='"${repoID}"';")
    # echo "Here is output:"
    # echo "$output"
    namespace=""
    if [ ! -z "$output" ]; then
        echo "Record found in director.repo_names"
        namespace=$(echo $output | awk 'FNR == 1 {print $5}')
    else 
        echo "No matches director.repo_names..."
        output=$(mysql -proot -h mariadb -u root --port 3306 -e "select * from tuf_reposerver.repo_namespaces where repo_id='"${repoID}"';")
        # echo "here is output: "
        # echo $output
        if [ -z "$output" ]; then
            echo "Ok, failed to find a match in either director or reposerver dbs! Skipping record!"
            continue
        fi
        echo "Record found in tuf_reposerver.repo_namespaces"
        namespace=$(echo $output | awk 'FNR == 1 {print $5}')
    fi
    echo "Confirm:"
    echo "Send key (re)generation request under namespace: $namespace and repoID: $repoID? (y|n)"
    read SEND
    if [ "$SEND" = "y" ]; then
        curl -H "x-ats-namespace:$namespace" -X PUT http://tuf-keyserver/api/v1/root/$repoID || {
            echo "Key Generation request failed!"
        }
    else
        echo "Skipping at users request"
    fi
    echo ""
    echo ""
done
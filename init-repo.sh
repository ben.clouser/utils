#!/usr/bin/env bash

OSTREE_REPO="ostree_repo"
OSTREE_ROOTFS="ostree_rootfs"
OSTREE_BRANCHNAME="master"
OSTREE_COMMIT_SUBJECT="manual commit from script"
OSTREE_COMMIT_BODY="Committed by manually performing the step that our OE builds usually do"

## Create the ostree_repo from the rootfs that has been tailored for ostree


## Create the ostree repo directory
ostree --repo=${OSTREE_REPO} init --mode=archive-z2

## Commit the result
ostree --repo=${OSTREE_REPO} commit \
       --tree=dir=${OSTREE_ROOTFS} \
       --skip-if-unchanged \
       --branch=${OSTREE_BRANCHNAME} \
       --subject="${OSTREE_COMMIT_SUBJECT}" \
       --body="${OSTREE_COMMIT_BODY}"



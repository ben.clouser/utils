#!/usr/bin/env bash

export TEST_ENV=dev2
export ENDPOINT_TEST_ONLY=true

export APP_SERVER_URL="https://app-dev2.int.toradex.com"
export API_GATEWAY_URL="https://api-dev2.int.toradex.com"
export DEVICE_GATEWAY_URL="https://ota-ce-dev2.int.toradex.com"
export JWT_UTIL_DIR=${JWT_UTIL_DIR}/jwt-util
# UUID added to test user's token
export UUID="167d7cda-2072-11ea-8733-5f9850348ac9"

python3 -m pytest -rA --capture=no test_endpoints.py

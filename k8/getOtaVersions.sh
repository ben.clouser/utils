#!/usr/bin/env bash


deployments=( ota-tdx-api ota-tdx-campaigner-daemon ota-tdx-campaigner ota-tdx-oauthful ota-tdx-web-events ota-tdx-reverse-proxy ota-tdx-metadata ota-tdx-device-gateway ota-tdx-tuf-keyserver-daemon ota-tdx-director-daemon ota-tdx-device-registry ota-tdx-utils ota-tdx-tuf-keyserver ota-tdx-tuf-reposerver ota-tdx-director ota-tdx-app ota-tdx-accounts )

for deployment in ${deployments[@]};do
	echo "$deployment: $(kubectl get deployment $deployment -o json | jq .spec.template.spec.containers[0].image)"
done

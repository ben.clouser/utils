#!/usr/bin/env bash
NAMESPACE="default"

RELEASE=${1:-"ota"}

OTA_DEPLOYMENTS=($(kubectl get deployment --selector=release=${RELEASE} |awk '!/NAME/ {print $1}'))
OTA_STATEFULSETS=($(kubectl get statefulsets --selector=release=${RELEASE} |awk '!/NAME/ {print $1}'))

echo "== Deployments:"
for i in "${!OTA_DEPLOYMENTS[@]}"; do
    name=${OTA_DEPLOYMENTS[$i]}
    echo "${name}:"
    kubectl get deployment ${name} -o json |jq .spec.template.spec.containers[0].image
done

echo ""
echo "== StatefulSets:"
for i in "${!OTA_STATEFULSETS[@]}"; do
    name=${OTA_STATEFULSETS[$i]}
    echo "${name}"
    kubectl get statefulset ${name} -o json |jq .spec.template.spec.containers[0].image
done

#!/usr/bin/env bash
route_add_linux() {
	export IFS=","
	for ip in $1; do
		sudo ip route add $ip dev ${TUNDEV} || {
			echo "Failed to add IP ($ip) to routes"
		}
	done
}

route_add_macos() {
	export IFS=","
	for ip in $1; do
		sudo route add -host $ip ${INTERNAL_IP4_ADDRESS} || {
			echo "Failed to add IP ($ip) to routes"
		}
	done
}

RANCHER_IP=$(/usr/bin/nslookup rancher.torizon.io 8.8.8.8 | tail -n 5 | awk '/Address:/ { print $2; }' | tr '\n' ',')
KONG_PROD=$(/usr/bin/nslookup app.torizon.io 8.8.8.8 | tail -n 5 | awk '/Address:/ { print $2; }' | tr '\n' ',')
KONG_PILOT=$(/usr/bin/nslookup app-pilot.torizon.io 8.8.8.8 | tail -n 5 | awk '/Address:/ { print $2; }' | tr '\n' ',')

echo ""
echo "Adding custom routes to vpn gw: $VPNGATEWAY (${TUNDEV})"
echo "internal ip4 address: ${INTERNAL_IP4_ADDRESS}"
echo "RANCHER_IP=${RANCHER_IP}"
echo "KONG_PROD=${KONG_PROD}"
echo "KONG_PILOT=${KONG_PILOT}"

if [ "$OS" = "Linux" ]; then
	route_add_linux $KONG_PROD
	route_add_linux $KONG_PILOT
else
	# MacOS
	route_add_macos $KONG_PROD
	route_add_macos $KONG_PILOT
fi

echo "Done Adding custom routes"
